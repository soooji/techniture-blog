// var path = require("path");
var bodyParser = require("body-parser");
var cors = require('cors')
const mongoose = require('mongoose');
var express = require("express");
var app = express();
app.listen(8081, err => {
    if (err) throw err
    console.log(`> Ready On Server`)
});
app.use(cors())
app.use(bodyParser.json());
//add static files
app.use(express.static(__dirname + '/build'));
app.use('/', express.static(__dirname + '/build'));
app.use(express.static(__dirname + '/build/static'));
app.use('/static', express.static(__dirname + '/build/static'));
app.use(express.static(__dirname + '/build/static/css'));
app.use('/static/css', express.static(__dirname + '/build/static/css'));
app.use(express.static(__dirname + '/build/static/js'));
app.use('/static/js', express.static(__dirname + '/build/static/js'));
app.use(express.static(__dirname + '/build/static/media'));
app.use('/static/media', express.static(__dirname + '/build/static/media'));
// add index
app.get('/', function (req, res) {
    res.sendfile('build/index.html');
});
// connect to database
mongoose.connect('mongodb://techniture:Sajad1378@ds135974.mlab.com:35974/techniture-blog', {useNewUrlParser: true});
mongoose.Promise = global.Promise;
// schemas
const PostSchema = mongoose.Schema({
    title: String
});
const Post = mongoose.model('Post', PostSchema);
// methods
app.get("/api/posts", function(req, res) {
    Post.find().sort({ no: -1 }).select('title img no')
    .then(posts => {
        res.send(posts);
    }).catch(err => {
        res.status(500).send({
            message: err.message
        });
    });
});

app.get("/api/posts/:id", function(req, res) {
    Post.findById(req.params.id).select('title description img')
    .then(post => {
        res.send(post);
    }).catch(err => {
        res.status(500).send({
            message: err.message
        });
    });
});

export default app;