import React, { Component } from 'react';
import axios from 'axios'
import Image from 'react-render-image';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      posts : [],
      currentPost: {
        description: '',
        title: '',
        id: 0,
        img: ''
      }
    }
  }
  componentDidMount() {
    this.getPosts();
  }
  getPosts() {
    axios.get('/api/posts')
    .then(r => {
      this.setState({posts: r.data})
    })
    .catch(e => console.log(e))
  }
  getPost(id) {
    this.setState({modal: true});
    document.getElementById("mdl").scrollTop = 0;
    document.getElementById("bdy").style.overflow = "hidden";
    axios.get(`/api/posts/${id}`)
    .then(r => {
        this.setState({currentPost: r.data})
    })
    .catch(e => console.log(e))
  }
  createMarkup() {
    return {__html: this.state.currentPost.description};
  }
  closePost() {
    this.setState({modal: false});
    this.setState({
      currentPost: {
        description: '',
        title: '',
        id: 0,
        img: ''
      }
    });
    document.getElementById("mdl").scrollTop = 0;
    document.getElementById("bdy").style.overflow = "auto";
  }
  render() {
    return (
      <main>
            <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/"
        crossorigin="anonymous"/>
          <link type="text/css" rel="stylesheet" href="https://cdn.rawgit.com/rastikerdar/shabnam-font/v4.0.0/dist/font-face.css"/>
        <div class="modal-bg" id="md" style={{display: this.state.modal ? 'block' : 'none'}}>
        <div class="modal" dir="rtl" id="mdl">
            <aside class="post">
                {/* <img src={this.state.currentPost.img} /> */}
                <Image src={this.state.currentPost.img} loading={<img src="http://sooji.ir/techniture/img/loading.png"/>} errored={<img src="http://sooji.ir/techniture/img/loading.png"/>}>
                  {({image, loaded, errored}) => {
                    return <img src={image.src}/>;
                  }}
                </Image>
                <div class="title">
                    {this.state.currentPost.title}
                </div>
                <div dangerouslySetInnerHTML={this.createMarkup()} class="desc"/>
            </aside>
        </div>
    </div>
    <div class="headerbg">
        <header>
            {
              !this.state.modal ?
              <a href="http://instagram.com/techniture" target="_blink"><i id="insta" class="fab fa-instagram"></i></a> :
              <i id="back" style={{paddingLeft:12,paddingRight:12}} onClick={()=>this.closePost()} class="fas fa-times"></i>
            }
            <a href="http://techniture.ir"><img src="http://sooji.ir/techniture/logo.svg" /></a>
            <i class="fas fa-bars"></i>
        </header>
    </div>
    <aside class="posts" id="renderedposts">
    {
      this.state.posts.map(
        (v, k) =>
          <aside key={k} class="post" onClick={()=>this.getPost(v._id)}><img src={v.img}/></aside>
      )
    }
    </aside>
    <footer>
        www.techniture.ir
    </footer>
      </main>
    );
  }
}

export default App;
{/* <aside class="post" onclick="openModal()"><img src="http://sooji.ir/techniture/img/asphalt.png"/></aside> */}