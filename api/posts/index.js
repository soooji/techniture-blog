// var path = require("path");
var bodyParser = require("body-parser");
var cors = require('cors')
const mongoose = require('mongoose');
var express = require("express");
var app = express();
app.use(cors())
app.use(bodyParser.json());

mongoose.connect('mongodb://techniture:Sajad1378@ds135974.mlab.com:35974/techniture-blog', {useNewUrlParser: true});
mongoose.Promise = global.Promise;
 
const PostSchema = mongoose.Schema({
    title: String
});
const Post = mongoose.model('Post', PostSchema);

app.get("/api/posts", function(req, res) {
    Post.find().select('title img')
    .then(posts => {
        res.send(posts);
    }).catch(err => {
        res.status(500).send({
            message: err.message
        });
    });
});

app.get("/api/posts/:id", function(req, res) {
    Post.findById(req.params.id).select('title description img')
    .then(post => {
        res.send(post);
    }).catch(err => {
        res.status(500).send({
            message: err.message
        });
    });
});

export default app;