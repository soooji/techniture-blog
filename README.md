# techniture-blog
## This project is like a boilerplate and start up project using
- NodeJS
- ReactJS
- MongoDB

ReactJS build folder is connected statically to NodeJS via **ExpressJS** and Nodejs is connected to the **mLab** mongoDB database with **mongoose**.
